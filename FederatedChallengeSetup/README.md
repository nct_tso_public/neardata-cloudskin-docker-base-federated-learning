## Dataset for Demo
The dataset used for the demo is the [CIFAR10](https://www.cs.toronto.edu/~kriz/cifar.html) dataset.
Direct Downloadlink can be found [here](https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz).
The dataset is downloaded and stored in the `data\clientX` folders.
You can run following script to download the dataset in the correct folders:
```bash
python FederatedChallengeSetup/data/setup_data.py
```


## Usage
```bash
docker build -t fl_challenge:team_name .
```

Start federated learning 
```bash
docker-compose up .
```

For actual challenge setup change settings in docker-compose.yaml to:
This setting permits or forbits docker to create a connection to the world wide web. 
```yaml
[...]
networks:
  challenge_net:
    internal: true
[...]
```
otherwise:
```yaml
[...]
networks:
  challenge_net:
    internal: false
[...]
```
